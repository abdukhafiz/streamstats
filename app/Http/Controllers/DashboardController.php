<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\GameRepositoryInterface;
use App\Repositories\Contracts\StreamRepositoryInterface;
use App\Repositories\Contracts\TagRepositoryInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /** @var \App\Repositories\StreamRepository $streamRepo */
    private $streamRepo;

    /** @var \App\Repositories\GameRepository $gameRepo */
    private $gameRepo;

    /** @var \App\Repositories\TagRepository $tagRepo */
    private $tagRepo;

    public function __construct(StreamRepositoryInterface $streamRepo, GameRepositoryInterface $gameRepo, TagRepositoryInterface $tagRepo)
    {
        $this->streamRepo = $streamRepo;
        $this->gameRepo = $gameRepo;
        $this->tagRepo = $tagRepo;
    }

    function dashboard(Request $request)
    {
        $orderBy = 'desc';
        if (!is_null($request->orderBy))
            $orderBy = $request->orderBy;

        $curUser = auth()->user();
        $userId = $curUser->id;

        $usersFollowingGames = $this->gameRepo->getGamesOfUser($userId);
        $topGamesByViewerCount = $this->gameRepo->topGamesByViewerCount($userId);
        $medianNumberOfStreams = $this->streamRepo->medianNumberOfViewers($userId);
        $top100StreamsByViewerCount = $this->streamRepo->getStreams(null, 100, $orderBy);
        $totalNearestHourStreams = $this->streamRepo->nearestNumberOfStreams();
        $followingFromTop1000 = $this->streamRepo->followingFromTop1000($userId);

        $needToGainInTop = $this->streamRepo->howManyViewersNeedToGainInTop1000($userId);
        if ($needToGainInTop <= 0) {
            $needToGainInTop = 'All of following streams of user are in the top 1000';
        }

        $tagsOfFollowingStreams = $this->tagRepo->getUsersFollowingStreamsTags($userId);

        return view('dashboard', compact(
            'usersFollowingGames',
            'topGamesByViewerCount',
            'medianNumberOfStreams',
            'top100StreamsByViewerCount',
            'totalNearestHourStreams',
            'followingFromTop1000',
            'needToGainInTop',
            'tagsOfFollowingStreams'
        ));
    }

}
