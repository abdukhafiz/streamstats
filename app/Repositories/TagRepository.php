<?php

namespace App\Repositories;

use App\Classes\Twitch\Tag\ImportTags;
use App\Repositories\Contracts\TagRepositoryInterface;

class TagRepository extends Repository implements TagRepositoryInterface
{
    function model()
    {
        return \App\Models\Tag::class;
    }

    function importTags($token, $stream, $insertedStream)
    {
        if (array_key_exists('tag_ids', $stream) && count($stream['tag_ids']) > 0) {
            $mappedTagID = [];
            foreach ($stream['tag_ids'] as $tagId) {
                if ($this->findBy('twitch_tag_id', $tagId)) {
                    continue;
                }
                array_push($mappedTagID, $tagId);
            }

            if (count($mappedTagID) > 0) {
                $importTags = new ImportTags($token, $stream['tag_ids']);
                $importTags->import();
                $tagIDs = $importTags->tagIDs;

                if (is_array($tagIDs)) {
                    $insertedStream->tags()->sync($tagIDs);
                }
            }
        }
    }

    /**
     * Insert tags (from twitch) with language
     * @param $tags
     * @return array
     */
    function insertWithLang($tags)
    {
        $tagIDs = [];
        if (is_array($tags) && count($tags) > 0) {
            foreach ($tags as $tag) {
                $insertedTag = $this->firstOrCreate([
                    'twitch_tag_id' => $tag['tag_id']
                ]);

                if (is_array($tag['localization_names']) && count($tag['localization_names']) > 0) {
                    foreach ($tag['localization_names'] as $key => $locName) {
                        $this->insertLang($insertedTag, $key, $locName);
                    }
                }

                array_push($tagIDs, $insertedTag->id);
            }
        }

        return $tagIDs;
    }

    /**
     * Insert tag language
     * @param $tag
     * @param $langCode
     * @param $tagName
     * @return mixed
     */
    private function insertLang($tag, $langCode, $tagName)
    {
        $tag->langs()->updateOrCreate([
            'tag_id' => $tag->id,
            'lang' => $langCode,
            'name' => $tagName
        ]);
    }

    /**
     * List of tags user's following streams
     * @param $userId
     * @return mixed
     */
    function getUsersFollowingStreamsTags($userId)
    {
        return $this->model
            ->with('langs')
            ->whereHas('streams', function ($query) use ($userId) {
                $query->usersFollowingStream($userId);
            })
            ->get();
    }
}
