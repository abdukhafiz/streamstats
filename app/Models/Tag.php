<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['twitch_tag_id'];

    function langs()
    {
        return $this->hasMany(\App\Models\TagLang::class);
    }

    function streams()
    {
        return $this->belongsToMany(\App\Models\Stream::class, 'tag_streams');
    }
}
