<?php

namespace App\Classes\Twitch\API;

interface RequestInterface
{

    function request();

}
