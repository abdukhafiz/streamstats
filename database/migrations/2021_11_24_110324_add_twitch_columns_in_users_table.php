<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwitchColumnsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('twitch_id')
                ->nullable()
                ->after('password');
            $table->string('twitch_username')
                ->nullable()
                ->after('twitch_id');
            $table->string('twitch_token')
                ->nullable()
                ->after('twitch_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('twitch_id');
            $table->dropColumn('twitch_username');
            $table->dropColumn('twitch_token');
        });
    }
}
