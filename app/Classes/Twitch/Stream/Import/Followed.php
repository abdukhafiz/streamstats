<?php

namespace App\Classes\Twitch\Stream\Import;

use App\Classes\Twitch\API\GET;
use App\Classes\Twitch\API\Import;

class Followed extends Import
{

    const FOLLOWED_STREAMS = 'streams/followed';

    private $user;

    public function __construct($user)
    {
        $this->user = $user;
        parent::__construct(resolve(\App\Repositories\Contracts\StreamRepositoryInterface::class));
    }

    /**
     * Tags followed stream of signed user
     */
    function import()
    {
        $getRequest = new GET($this->user->twitch_token, self::FOLLOWED_STREAMS, $this->params);

        if ($getRequest->statusCode == 200) {
            $body = $getRequest->body;

            $streams = $body['data'];
            if (count($streams) > 0) {
                $this->repo->importFollowingStreams($streams, $this->user);
            }
        } else {
            throw new \Exception();
        }
    }

    /**
     * Setting request params
     */
    function setParams()
    {
        $this->params = [
            'user_id' => $this->user->twitch_id
        ];
    }
}
