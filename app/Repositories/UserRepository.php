<?php

namespace App\Repositories;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository extends Repository implements UserRepositoryInterface
{
    function model()
    {
        return \App\Models\User::class;
    }

    /**
     * Creating new user
     * @param $name
     * @param $email
     * @param null $twitchId
     * @param null $twitchUsername
     * @param null $twitchToken
     * @return mixed
     */
    function storeUser($name, $email, $twitchId = null, $twitchUsername = null, $twitchToken = null)
    {
        return $this->create([
            'name' => $name,
            'email' => $email,
            'twitch_id' => $twitchId,
            'twitch_username' => $twitchUsername,
            'twitch_token' => $twitchToken
        ]);
    }

    /**
     * Is twitch user exists
     * @param $twitchId
     * @return mixed
     */
    function isTwitchUserExists($twitchId)
    {
        return $this->findBy('twitch_id', $twitchId);
    }

    /**
     * Refresh twitch token
     * @param $twitchId
     * @param $token
     * @return mixed
     */
    function refreshToken($twitchId, $token)
    {
        return $this->updateByIn([$twitchId], 'twitch_id', [
            'twitch_token' => $token
        ]);
    }
}
