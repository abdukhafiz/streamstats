<?php

namespace App\Classes\Twitch\API;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;

class General
{

    /** @var string Base API url */
    private $baseUrl = 'https://api.twitch.tv/helix/';
    /** @var string Twitch token */
    private $token;

    protected $response;

    public function __construct($token)
    {
        $this->token = $token;
        $this->setResponse();
    }

    private function setResponse()
    {
        $this->response = Http::withHeaders(
            [
                'Client-Id' => env('TWITCH_CLIENT_ID')
            ])
            ->withToken($this->token)
            ->retry(3, 30000, function ($exception) {
                return $exception instanceof ConnectionException;
            });
    }

    protected function preparedUrl($url)
    {
        return $this->baseUrl . $url;
    }

}
