<?php

namespace App\Classes\Twitch\API;

class GET extends General implements RequestInterface
{

    private $url;
    private $queryParam;

    public $body;
    public $statusCode;

    public function __construct($userToken, $url, $queryParam)
    {
        parent::__construct($userToken);

        $this->queryParam = $queryParam;
        $this->url = $this->preparedUrl($url);
        $this->request();
        $this->setBody();
        $this->setStatus();
    }

    private function setBody()
    {
        $this->body = $this->response->json();
    }

    private function setStatus()
    {
        $this->statusCode = $this->response->status();
    }

    /**
     * GET request
     * @return void
     */
    function request()
    {
        $this->response = $this->response->get($this->url, $this->queryParam);
    }

}
