<?php

namespace App\Repositories;

use App\Repositories\Contracts\StreamRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StreamRepository extends Repository implements StreamRepositoryInterface
{
    function model()
    {
        return \App\Models\Stream::class;
    }

    /**
     * Insert stream
     * @param $stream
     * @param $gameId
     * @return mixed
     */
    function insert($stream, $gameId)
    {
        return $this->updateOrCreate(
            ['twitch_stream_id' => $stream['id']],
            [
                'game_id' => $gameId,
                'channel_name' => $stream['user_name'],
                'title' => $stream['title'],
                'started_time' => Carbon::parse($stream['started_at'])->toDateTimeString(),
                'viewers_count' => $stream['viewer_count']
            ]
        );
    }

    /**
     * Tags following streams
     * @param array $streams
     * @param $user
     */
    function importFollowingStreams(array $streams, $user)
    {
        /** @var \App\Repositories\GameRepository $gameRepo */
        $gameRepo = resolve(\App\Repositories\Contracts\GameRepositoryInterface::class);

        /** @var \App\Repositories\TagRepository $tagRepo */
        $tagRepo = resolve(\App\Repositories\Contracts\TagRepositoryInterface::class);

        $streamIDs = [];
        foreach ($streams as $stream) {
            // insert game of following streams
            $game = $gameRepo->insertGame([
                'twitch_game_id' => $stream['game_id'],
                'name' => $stream['game_name']
            ]);

            $insertedStream = $this->insert($stream, $game->id);

            $tagRepo->importTags($user->twitch_token, $stream, $insertedStream);

            array_push($streamIDs, $insertedStream->id);
        }

        $user->followingStreams()->sync($streamIDs);
    }

    /**
     * Get streams
     * @param null $userId
     * @param null $limit
     * @param null $sortByViewerCount
     * @return mixed
     */
    function getStreams($userId = null, $limit = null, $sortByViewerCount = null)
    {
        $query = $this->model;

        if (!is_null($userId)) {
            $query = $query->usersFollowingStream($userId);
        }

        if (!is_null($limit)) {
            $query = $query->take($limit);
        }

        if (!is_null($sortByViewerCount) && in_array($sortByViewerCount, ['asc', 'desc'])) {
            $query = $query->orderBy('viewers_count', $sortByViewerCount);
        }

        return $query->get();
    }

    /**
     * Get median number of viewers for user's following streams
     * @param $userId
     * @return float|int|mixed
     */
    function medianNumberOfViewers($userId)
    {
        $viewersCountArr = $this->getStreams($userId)
            ->pluck('viewers_count')
            ->toArray();

        return getMedianNumber($viewersCountArr);
    }

    /**
     * Total number of nearest started streams (in the range of last one hour)
     * @return mixed
     */
    function nearestNumberOfStreams()
    {
        $curTime = now();

        return $this->model
            ->where([
                ['started_time', '>=', $curTime->subHour()->toDateTimeString()],
                ['started_time', '<=', $curTime->toDateTimeString()],
            ])->count();
    }

    /**
     * Get following streams from top 1000
     * @param $userId
     * @return mixed
     */
    function followingFromTop1000($userId)
    {
        return $this->model
            ->whereHas('userFollowingStreams', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->get();
    }

    /**
     * Get top 1000 streams
     * @return mixed
     */
    function top1000Streams()
    {
        return $this->model
            ->orderBy(DB::raw("DATE_FORMAT(started_time, '%Y-%m-%d')"), 'desc')
            ->orderBy('viewers_count', 'desc')
            ->take(1000)
            ->get();
    }

    /**
     * How many viewers do need to get lowest user's following stream into the top 1000 streams
     * @param $userId
     * @return int
     */
    function howManyViewersNeedToGainInTop1000($userId)
    {
        $lowestViewerFromTop1000 = $this->top1000Streams()->last();
        $lowestViewerFromFollowing = $this->getStreams($userId, 1, 'sortByViewerCount')->first();

        if(!is_null($lowestViewerFromTop1000) && !is_null($lowestViewerFromFollowing)) {
            return $lowestViewerFromTop1000->viewers_count - $lowestViewerFromFollowing->viewers_count;
        }
        return 0;
    }

}
