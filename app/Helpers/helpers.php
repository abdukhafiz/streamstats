<?php

if (!function_exists('getMedianNumber')) {
    function getMedianNumber(array $array)
    {
        sort($array);
        $count = count($array);

        $middleValue = floor(($count - 1) / 2);
        if ($count % 2) {
            return $array[$middleValue];
        } else {
            $firstValue = $array[$middleValue];
            $secondValue = $array[$middleValue + 1];
            return (($firstValue + $secondValue) / 2);
        }
    }
}
