<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagLang extends Model
{
    use HasFactory;

    protected $fillable = ['tag_id', 'lang', 'name'];

    function tag()
    {
        return $this->belongsTo(\App\Models\Tag::class);
    }
}
