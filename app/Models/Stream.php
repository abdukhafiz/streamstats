<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    use HasFactory;

    protected $fillable = ['game_id','twitch_stream_id','channel_name','title','started_time','viewers_count'];

    function game()
    {
        return $this->belongsTo(\App\Models\Game::class);
    }

    function userFollowingStreams()
    {
        return $this->belongsToMany(\App\Models\User::class, 'following_streams');
    }

    function scopeUsersFollowingStream($query, $userId)
    {
        return $query->whereHas('userFollowingStreams', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        });
    }

    function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class, 'tag_streams');
    }
}
