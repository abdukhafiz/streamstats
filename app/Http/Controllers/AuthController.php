<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    /** @var \App\Repositories\UserRepository $userRepo */
    private $userRepo;

    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    function loginWithTwitch()
    {
        return Socialite::driver('twitch')
            ->scopes(['user:read:follows', 'analytics:read:games'])
            ->redirect();
    }

    function authCallback()
    {
        $twitchUser = Socialite::driver('twitch')
            ->user();

        if (is_null($twitchUser))
            return redirect(404);

        $twitchUserId = $twitchUser->getId();
        $twitchUserEmail = $twitchUser->getEmail();
        $twitchUserName = $twitchUser->getName();
        $twitchUserLogin = $twitchUser->user['login'];
        $twitchUserToken = $twitchUser->token;

        $user = $this->userRepo->isTwitchUserExists($twitchUser->getId());

        if (is_null($user)) {
            $user = $this->userRepo->storeUser($twitchUserName, $twitchUserEmail, $twitchUserId, $twitchUserLogin, $twitchUserToken);
        }

        $this->userRepo->refreshToken($twitchUserId, $twitchUserToken);

        Auth::login($user);
        return redirect(RouteServiceProvider::HOME);
    }

}
