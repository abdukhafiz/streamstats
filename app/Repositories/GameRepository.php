<?php

namespace App\Repositories;

use App\Repositories\Contracts\GameRepositoryInterface;

class GameRepository extends Repository implements GameRepositoryInterface
{
    function model()
    {
        return \App\Models\Game::class;
    }

    function insertGame($game)
    {
        return $this->updateOrCreate(
            ['twitch_game_id' => $game['twitch_game_id']],
            ['name' => $game['name']]
        );
    }

    /**
     * Get list of games with total number of streams (from top 1000)
     * @param $userId
     * @return mixed
     */
    function getGamesOfUser($userId)
    {
        return $this->model
            ->withCount('streams')
            ->usersFollowingStream($userId)
            ->get();
    }

    /**
     * List of top games by viewer count
     * @param $userId
     * @return mixed
     */
    function topGamesByViewerCount($userId)
    {
        return $this->model
            ->withSum('streams', 'viewers_count')
            ->usersFollowingStream($userId)
            ->orderBy('streams_sum_viewers_count', 'desc')
            ->get();
    }
}
