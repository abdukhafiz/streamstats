<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = ['twitch_game_id', 'name'];

    function streams()
    {
        return $this->hasMany(\App\Models\Stream::class);
    }

    function scopeUsersFollowingStream($query, $userId)
    {
        return $query->whereHas('streams', function ($query) use ($userId) {
            $query->whereHas('userFollowingStreams', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            });
        });
    }
}
