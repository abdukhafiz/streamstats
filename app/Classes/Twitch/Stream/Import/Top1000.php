<?php

namespace App\Classes\Twitch\Stream\Import;

use App\Classes\Twitch\API\GET;
use App\Classes\Twitch\API\Import;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Top1000 extends Import
{

    const STREAMS_URL = 'streams';

    private $twitchToken;

    public function __construct($twitchToken)
    {
        parent::__construct(resolve(\App\Repositories\Contracts\StreamRepositoryInterface::class));

        $this->twitchToken = $twitchToken;
    }

    /**
     * Tags top 1000 streams
     */
    function import()
    {
        Log::info('Tags 1000 top streams started');

        for ($i = 0; $i < 10; $i++) {
            $request = new GET($this->twitchToken, self::STREAMS_URL, $this->params);

            if ($request->statusCode == 200) {
                $body = $request->body;
                $streams = $request->body['data'];

                $this->insertStreams($streams);

                if (array_key_exists('pagination', $body)) {
                    $pagination = $body['pagination'];
                    if (array_key_exists('cursor', $pagination)) {
                        $this->params['after'] = $pagination['cursor'];

                        // if cursor empty exit from loop
                        if (empty($this->params['after'])) {
                            break;
                        }
                    }
                }
            } else {
                Log::info('Request error: import 1000 top streams');
                throw new \Exception();
            }
        }

        Log::info('Tags 1000 top streams finished');
    }

    /**
     * Insert streams
     * @param array $streams
     */
    private function insertStreams(array $streams)
    {
        /** @var \App\Repositories\TagRepository $tagRepo */
        $tagRepo = resolve(\App\Repositories\Contracts\TagRepositoryInterface::class);

        foreach ($streams as $stream) {
            $game = $this->getGame($stream['game_id'], $stream['game_name']);

            $insertedStream = $this->repo->insert($stream, $game->id);
            $tagRepo->importTags($this->twitchToken, $stream, $insertedStream);
        }
    }

    /**
     * Get game by Game Twitch ID
     * @param $twitchGameID
     * @param $gameName
     * @return mixed
     */
    private function getGame($twitchGameID, $gameName)
    {
        /** @var \App\Repositories\GameRepository $gameRepo */
        $gameRepo = resolve(\App\Repositories\Contracts\GameRepositoryInterface::class);

        return $gameRepo->firstOrCreate([
            'twitch_game_id' => $twitchGameID
        ], [
            'name' => $gameName
        ]);
    }

    /**
     * GET request params
     */
    function setParams()
    {
        $this->params = [
            'first' => 100,
            'after' => ''
        ];
    }
}
