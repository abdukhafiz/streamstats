<?php

namespace App\Classes\Twitch\Tag;

use App\Classes\Twitch\API\GET;
use App\Classes\Twitch\API\Import;

class ImportTags extends Import
{
    const IMPORT_TAG = 'tags/streams';

    private $token;
    private $tagIds;

    public $tagIDs = [];

    public function __construct($token, array $tagIds)
    {
        $this->token = $token;
        $this->tagIds = $tagIds;
        parent::__construct(resolve(\App\Repositories\Contracts\TagRepositoryInterface::class));
    }

    /**
     * Tags tag by tag ID
     */
    function import()
    {
        $getRequest = new GET($this->token, self::IMPORT_TAG, $this->params);

        if ($getRequest->statusCode == 200) {
            $body = $getRequest->body;

            $tags = $body['data'];
            $this->tagIDs = $this->repo->insertWithLang($tags);
        } else {
            throw new \Exception();
        }
    }

    /**
     * Setting request params
     */
    function setParams()
    {
        $strParam = '';
        foreach ($this->tagIds as $tagId) {
            $strParam .= 'tag_id=' . $tagId . '&';
        }
        $this->params = $strParam;
    }
}
