<?php

namespace App\Classes\Twitch\Game\Import;

use App\Classes\Twitch\API\GET;
use App\Classes\Twitch\API\Import;
use App\Repositories\Contracts\GameRepositoryInterface;
use Illuminate\Support\Facades\Log;

class Top100 extends Import
{
    const TOP100_URL = 'games/top';

    private $twitchToken;

    public function __construct($twitchToken)
    {
        parent::__construct(resolve(\App\Repositories\Contracts\GameRepositoryInterface::class));

        $this->twitchToken = $twitchToken;
    }

    /**
     * Tags top 100 games
     */
    function import()
    {
        Log::info('Tags 100 top games started');

        $request = new GET($this->twitchToken, self::TOP100_URL, $this->params);

        if ($request->statusCode == 200) {
            $gamesArr = $request->body['data'];

            foreach ($gamesArr as $game) {
                $this->insertGame($game);
            }

            Log::info('Tags 100 top games finished');
        } else {
            Log::info('Request error: import 100 top games');
            throw new \Exception();
        }
    }

    /**
     * Insert or update (if exists) game
     * @param $game
     */
    private function insertGame($game)
    {
        $this->repo->updateOrCreate([
            'twitch_game_id' => $game['id']
        ], [
            'name' => $game['name']
        ]);
    }

    /**
     * Setting params
     */
    function setParams()
    {
        $this->params = [
            'first' => 100
        ];
    }
}
