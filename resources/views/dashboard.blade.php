<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="row">
                        <h3>List of user's following games with total number of streams (from top 1000)</h3>
                        <ul>
                            @foreach($usersFollowingGames as $value)
                                <li>Game name: <strong>{{$value->name}}</strong> - Streams count: <strong>{{$value->streams_count}}</strong></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="row">
                        <h3>List of user's following games by viewer count (following streams)</h3>
                        <ul>
                            @foreach($topGamesByViewerCount as $topGames)
                                <li>Game name: <strong>{{$topGames->name}}</strong> - Sum of viewers count: <strong>{{$topGames->streams_sum_viewers_count}}</strong></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="row">
                        <h3>Median number of viewers for user's following streams</h3>
                        <div>Median number: <strong>{{$medianNumberOfStreams}}</strong></div>
                    </div>

                    <div class="row">
                        <h3>Total number of nearest started streams (in the range of last one hour)</h3>
                        <div>{{$totalNearestHourStreams}}</div>
                    </div>

                    <div class="row">
                        <h3>How many viewers do need to get lowest user's following stream into the top 1000 streams</h3>
                        <div>{{$needToGainInTop}}</div>
                    </div>

                    <div class="row">
                        <h3>Which tags are shared between the user followed streams and the top 1000 streams? Also make sure to translate the tags to their respective name.</h3>
                        <ul>
                            @foreach($tagsOfFollowingStreams as $value)
                                <li>
                                    <p>Twitch Tag ID: <strong>{{$value->twitch_tag_id}}</strong></p>
                                    <p>{{implode($value->langs->pluck('name')->toArray(), ', ')}}</p>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <div class="row">
                    <h3>List of top 100 streams by viewer count that can be sorted asc & desc</h3>
                    <a href="{{route('dashboard', ['orderBy' => 'asc'])}}">Click for ASC sort</a><br />
                    <a href="{{route('dashboard', ['orderBy' => 'desc'])}}">Click for DESC sort</a><br /><br />

                    <ul>
                        @foreach($top100StreamsByViewerCount as $key => $value)
                            <li>{{$key+1}}. {{$value->title}} | <strong>Viewer counts</strong>: {{$value->viewers_count}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" style="padding-top: 50px;">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <div class="row">
                    <h3>Which of the top 1000 streams is the logged in user following?</h3>
                    <ul>
                        @foreach($followingFromTop1000 as $key => $value)
                            <li>{{$key+1}}. {{$value->title}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>

<style>
    h3 {
        font-weight: bold;
        font-size: 20px;
        padding-bottom: 10px;
    }

    .row {
        border-bottom: 1px solid rgb(215, 214, 214);
        padding: 15px 0;
    }
</style>
