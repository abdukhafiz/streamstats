<?php

namespace App\Classes\Twitch\API;

abstract class Import
{

    /** @var array Parameters of request */
    protected $params = [];

    protected $repo;

    public function __construct($repoInterface)
    {
        $this->repo = $repoInterface;
        $this->setParams();
    }

    abstract function import();

    /**
     * Setting request params
     */
    abstract function setParams();

}
