<?php

namespace App\Providers;

use App\Repositories\Contracts\GameRepositoryInterface;
use App\Repositories\Contracts\StreamRepositoryInterface;
use App\Repositories\Contracts\TagRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

use App\Repositories\GameRepository;
use App\Repositories\StreamRepository;
use App\Repositories\TagRepository;
use App\Repositories\UserRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public $bindings = [
        UserRepositoryInterface::class => UserRepository::class,
        GameRepositoryInterface::class => GameRepository::class,
        StreamRepositoryInterface::class => StreamRepository::class,
        TagRepositoryInterface::class => TagRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
