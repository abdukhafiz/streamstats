<?php

namespace App\Console;

use App\Classes\Twitch\Game\Import\Top100;
use App\Classes\Twitch\Stream\Import\Top1000;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        /**
         * Import top 100 games
         */
        $schedule->call(function () {
            $importGames = new Top100(env('TWITCH_TOKEN'));
            $importGames->import();
        })->everyFifteenMinutes();

        /**
         * Import top 1000 streams
         */
        $schedule->call(function () {
            $importStreams = new Top1000(env('TWITCH_TOKEN'));
            $importStreams->import();
        })->everyFifteenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
